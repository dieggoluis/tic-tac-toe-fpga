library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library pacotes;
use pacotes.pack.all;


-- Exibe informacoes sobre o jogo na placa
entity board_decoder is
	port (
		clk_game: in std_logic; -- clock do game
		game_mode: in std_logic; -- Modo de jogo: 0-Jogador vs Jogador
								 --				  1-Jogador vs AI
		en_result: in std_logic;    -- Habilita/desabilita resultado
		en_mode: in std_logic;    -- Habilita/desabilita chave que indica modo do jogo
		turn: in std_logic; -- Vez: 0- Primeiro Jogador, 1- Segundo jogador
		winner: in std_logic_vector(1 downto 0); -- Vencedor: 00 - nenhum resultado
												 --		   01 - segundo jogador
											     --		   10 - primeiro jogador
												     --		   11 - empate
		--test
		--clk27M: in std_logic;
		score_p1, score_p2: in std_logic_vector(3 downto 0); -- pontuacao dos jogadores
		
		-- saidas da placa
		LEDG: out std_logic_vector(7 downto 0); -- leds verdes
		LEDR: out std_logic_vector(7 downto 0); -- leds vermelhos
		HEX0,HEX1,HEX2,HEX3: out std_logic_vector(0 to 6 ) -- displays de 7 segmentos
				
	);
end board_decoder;

architecture behaviour of board_decoder is
	--test
	--signal clk_game: std_logic;
	signal clock_led: std_logic;
	signal hex0_score, hex3_score: std_logic_vector(6 downto 0);
begin
	clock_leds: clock_divider
		generic map (N => 2400000)
		port map ( 
		   Clock_in => clk_game,
		   Resetn => '1',
		   Clock_out => clock_led
		);
		
	conv1_component: conv_7seg
		port map(
			digit => score_p1,
			seg => hex0_score
		);

	conv2_component: conv_7seg
		port map(
			digit => score_p2,
			seg => hex3_score
		);

	process(clk_game)
	begin
		if(rising_edge(clk_game)) then

			if(turn = '0') then 
				LEDR <= (others => '1');
				LEDG <= (others => '0');
			else
				LEDG <= (others => '1');
				LEDR <= (others => '0');
			end if;

			if(en_mode = '1') then
				if(game_mode = '0') then
					HEX2 <= "0010010"; -- 2
					HEX1 <= "0011000"; -- P
				else
					HEX2 <= "0001000"; -- A
					HEX1 <= "1001111"; -- I
				end if;
			end if;

			HEX0 <= hex0_score;
			HEX3 <= hex3_score;

			if(en_result='1') then
				case winner is
					when "01"=>
						LEDG <= (others => clock_led);
						LEDR <= (others => '0');
						HEX3 <= "0100011"; -- W
						HEX2 <= "1101111"; -- I
						HEX1 <= "1101010"; -- N
						HEX0 <= "0100100"; -- S
					when "10"=>
						LEDR <= (others => clock_led);
						LEDG <= (others => '0');
						HEX3 <= "0100011"; -- W
						HEX2 <= "1101111"; -- I
						HEX1 <= "1101010"; -- N
						HEX0 <= "0100100"; -- S
					when "11"=>
						LEDR <= (others => '0');
						LEDG <= (others => '0');
						HEX3 <= "1111111"; -- display off
						HEX2 <= "1110000"; -- T
						HEX1 <= "1111001"; -- I
						HEX0 <= "0110000"; -- E
					when others=>
				end case;
			end if;
		end if;
	end process;
	
end behaviour;