library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library pacotes;
use pacotes.pack.all;

-- Atualiza estado do jogo e retorna game_board
entity game_state is
	port (
		clk_game: in std_logic; -- clock do game
		r, c: in integer range 0 to 3; -- linha e coluna 
		turn: in std_logic; -- primeiro jogador (esquerda): 0
							-- segundo jogador (direita): 1
		valid: in std_logic; -- validade da jogada, 0 para inv�lida e 1 para v�lida
		clear: in std_logic; -- limpa o tabuleiro
		
		num_col,num_row: out array_pair_ii;
		num_main_diag, num_sec_diag: out pair_ii;
		
		game_board: out array_2d -- tabuleiro que representa o estado do jogo
		
		
	);
end game_state;

architecture behaviour of game_state is
begin

	process (clk_game,valid,clear)
	variable v_num_col,v_num_row: array_pair_ii;
	variable v_num_main_diag, v_num_sec_diag: pair_ii;
	variable player: integer range 0 to 1;
	variable v_game_board: array_2d;
	begin
		if clk_game = '1' and clk_game'EVENT then
			if clear = '1'then
				-- reseta a diagonal principal
				v_num_main_diag(0) := 0;
				v_num_main_diag(1) := 0;
				
				-- reseta a diagonal secundaria
				v_num_sec_diag(0) := 0;
				v_num_sec_diag(1) := 0;
				for i in 0 to 2 loop
					--reseta a r-esima componente das contagens de linha e coluna
					v_num_row(i)(0) := 0;
					v_num_row(i)(1) := 0;
					
					v_num_col(i)(0) := 0;
					v_num_col(i)(1) := 0;
					for j in 0 to 2 loop
						v_game_board(i,j) := "00";
					end loop;
				end loop;
			
			
			elsif valid = '1' then
				if v_game_board(r,c) = "00" then
					if turn = '0' then
						player := 0;
					else
						player := 1;
					end if;
					
					if player = 0 then
						v_game_board(r,c) := "10";
					else
						v_game_board(r,c) := "01";
					end if;
					--atualiza contagem de pecas por jogador
					v_num_col(c)(player) := v_num_col(c)(player)+1; -- coluna
					v_num_row(r)(player) := v_num_row(r)(player)+1; -- linha
					if c = r then -- diagonal principal
						v_num_main_diag(player) := v_num_main_diag(player)+1;
					end if;
		
					if c + r = 2 then -- diagonal secundaria
						v_num_sec_diag(player) := v_num_sec_diag(player)+1;
					end if;
				end if;
			end if;			
			num_col <= v_num_col;
			num_row <= v_num_row;
			num_main_diag <= v_num_main_diag;
			num_sec_diag <= v_num_sec_diag;
			game_board <= v_game_board;
		end if;
	end process;
end behaviour;
