library ieee;
use ieee.std_logic_1164.all;

package custom_types is
  type array_2d is array(2 downto 0, 2 downto 0) of std_logic_vector(1 downto 0);
  type pair_ii is array(1 downto 0) of integer range 0 to 3;
  type array_pair_ii is array(2 downto 0) of pair_ii;
end custom_types;
