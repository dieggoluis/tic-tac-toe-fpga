LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY pacotes;
use pacotes.pack.all;

PACKAGE pack IS
	-- tipos definidos no projeto
	type array_2d is array(2 downto 0, 2 downto 0) of std_logic_vector(1 downto 0);
	type pair_ii is array(1 downto 0) of integer range 0 to 3;
	type array_pair_ii is array(2 downto 0) of pair_ii;
	
	component board_decoder is
	port (
		clk_game: in std_logic; 	-- clock do game
		game_mode: in std_logic; 	-- Modo de jogo: 0-Jogador vs Jogador
								 	--				 1-Jogador vs AI
		en_result: in std_logic; 	-- Habilita/desabilita displays que mostram o resultado do jogo
		en_mode: in std_logic;      -- Habilita/desabilita chave que indica modo do jogo
		turn: in std_logic; 		-- Vez: 0- Primeiro Jogador, 1- Segundo jogador
		winner: in std_logic_vector(1 downto 0); -- Vencedor: 00 - nenhum resultado
												 --		   01 - player 1 venceu
											     --		   10 - player 0 venceu
											     --		   11 - empate
		score_p1, score_p2: in std_logic_vector(3 downto 0); -- pontuacao dos jogadores
		
		-- saidas da placa
		LEDG: out std_logic_vector(7 downto 0); -- leds verdes
		LEDR: out std_logic_vector(7 downto 0); -- leds vermelhos
		HEX0,HEX1,HEX2,HEX3: out std_logic_vector(0 to 6 ) -- displays de 7 segmentos				
	);
	end component;
	
	component game_engine is
	port (
		clear: in std_logic;	-- nova partida ?
		clk_game: in std_logic; -- clock do game
		mode: in std_logic;		-- mode de jogo
		game_board: in array_2d;	-- tabuleiro
		AI_r, AI_c, player_r, player_c: in integer range 0 to 3; -- linha/coluna do tabuleiro que representa a jogada
		first_player: in std_logic; -- primeiro jogador0, segundo jogador: 1
		valid: out std_logic; -- jogada valida ?
							   
		player_move: out std_logic;
		r, c: out integer range 0 to 3; -- linha/coluna que serao inseridas
		turn: out std_logic; -- de qual jogador � a vez
		turn_cnt: out std_logic_vector(3 downto 0)
	);
	end component;
	
	component game_result is
	port (
		clk_game: in std_logic; -- clock do game
		game_board: in array_2d;
		state: out std_logic_vector(1 downto 0)  -- 00 nao houve empate e nem vencedor
												 -- 11 houve empate
												 -- 10 player 0 venceu
												 -- 01 player 1 venceu
	);
	end component;	
	
	component game_state is
	port (
		clk_game: in std_logic; -- clock do game
		r, c: in integer range 0 to 3; -- linha e coluna 
		turn: in std_logic; -- primeiro jogador (esquerda): 0
							-- segundo jogador (direita): 1
		valid: in std_logic; -- validade da jogada, 0 para inv�lida e 1 para v�lida
		clear: in std_logic; -- limpa o tabuleiro
	
		num_col,num_row: out array_pair_ii;			-- numero pe�as/simbolos em linhas de coluna
		num_main_diag, num_sec_diag: out pair_ii;	-- numero pe�as/simbolos em diagonais
		
		game_board: out array_2d -- tabuleiro que representa o estado do jogo
	);
	end component;
	
	component keyboard_decoder is
	port
	(
		------------------------	Clock Input	 	------------------------
		CLOCK_24	: 	in	std_logic_vector(1 downto 0);
									
		------------------------	PS2		--------------------------------
		PS2_DAT 	:		inout	STD_LOGIC;	--	PS2 Data
		PS2_CLK		:		inout	STD_LOGIC;		--	PS2 Clock
		
		pressed     : out std_logic;			-- pressionado alguma tecla ?
		event_square: out std_logic;			-- pressionado alguma tecla que representa um determinado quadrado do tabuleiro ?
		event_enter: out std_logic;				-- pressionado a tecla Enter ?
		square	: out integer range 0 to 9;		-- quadrado que representa a tecla pressionada
		reset 	: in std_logic;					
		en		: in std_logic 					-- enable para o keyboard
	);
	end component;
	
	component mode_decoder is
	port (
		clk_game: in std_logic; -- clock do game
		game_mode: out std_logic; -- modo de jogo? 0: player vs player, 1: player vs AI
		first_player: out std_logic; -- primeiro jogador? 0: esquerda, 1: direita
		
		en_decoder: in std_logic; -- enable para o decoder
		sw_mode: in std_logic; -- switch para o modo de jogo
		sw_player:in std_logic -- swith para o primeiro player
	);
	end component;
	
	component players is
	port ( 
		 clk_game: in std_logic; -- clock do game
		 square: in integer range 0 to 9; -- quadrado do tabuleiro que foi selecionado
         pressed: in std_logic;               -- pressed: 1
                                              -- not pressed: 0
         r, c: out integer range 0 to 3 -- row and column, respectively 
       );
	end component;
	
	component vga_decoder is
	port (
		game_board: in array_2d; -- tabuleiro que representa o estado do jogo
		resetn: in std_logic; -- low active
		en_draw: in std_logic; -- enable draw symbols
		is_drawing: out std_logic; -- is drawing ?
		CLOCK_24: in std_logic_vector(1 downto 0);  
		
		-- entradas e saidas da vga
		clk27M: in std_logic;
		VGA_R, VGA_G, VGA_B	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		VGA_HS, VGA_VS		: OUT STD_LOGIC
	);
	end component;
	
	component AI is
		port (
			clk_game: in std_logic; -- clock do game
			game_board: in array_2d; -- representa o tabuleiro 3x3
			
			-- contagem de pe�as
			num_col,num_row: in array_pair_ii; 
			num_main_diag, num_sec_diag: in pair_ii;
			
			r, c: out integer range 0 to 3 -- linha e coluna, respectivamente
		);
	end component;
	
	component clock_divider is
		generic (N: integer := 4);    -- N par -> Clock_out = Clock_in/N
								      -- N impar -> Clock_out = Clock_in/(N-1)
		port ( Clock_in: in std_logic;
		   Resetn: in std_logic;
		   Clock_out: out std_logic 
		 );
	end component;

	component conv_7seg IS
		PORT (digit : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			  seg : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
		);
		END component;
END PACKAGE;