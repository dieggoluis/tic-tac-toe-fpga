library ieee;
use ieee.std_logic_1164.all;

library pacotes;
use pacotes.pack.all;

-- Determina se houve vencedor, empate ou nada
entity game_result is
	port (
		clk_game: in std_logic; -- clock do game
		game_board: in array_2d;
		state: out std_logic_vector(1 downto 0)  -- 00 nao houve empate e nem vencedor
												 -- 11 houve empate
												 -- 10 primeiro player venceu
												 -- 01 segundo player venceu
	);
end game_result;

architecture behaviour of game_result is
begin
	process(clk_game)
	variable done: std_logic;
	begin
		if clk_game'EVENT and clk_game = '1' then
		
			--sup�e empate
			state <= "11";
			
			done := '0';
			-- Verifica linhas
			for r in 0 to 2 loop
				if game_board(r,0) = game_board(r,1) and game_board(r,1) = game_board(r,2) then
					if game_board(r,0) = "01" or game_board(r,0) = "10" then
						done := '1';
						state <= game_board(r,0);
					end if;
				end if;
			end loop;
			
			-- Verifica colunas
			for c in 0 to 2 loop
				if game_board(0,c) = game_board(1,c) and game_board(1,c) = game_board(2,c) then
					if game_board(0,c) = "01" or game_board(0,c) = "10" then
						done := '1';
						state <= game_board(0,c);
					end if;
				end if;
			end loop;
			
			-- Verifica diagonals
			if (game_board(0,0) = game_board(1,1) and game_board(1,1) = game_board(2,2)) or --principal
				(game_board(0,2) = game_board(1,1) and game_board(1,1) = game_board(2,0)) then --secundaria
				if game_board(1,1) = "01" or game_board(1,1) = "10" then
					done := '1';
					state <= game_board(1,1);
				end if;
			end if;
			
			-- se ninguem venceu, verifica se existe uma posicao vazia
			if done = '0' then
				for r in 0 to 2 loop
					for c in 0 to 2 loop
						if game_board(r,c) = "00" then
							state <= "00";
						end if;
					end loop;
				end loop;
			end if;
		
		end if;
	end process;
end behaviour;