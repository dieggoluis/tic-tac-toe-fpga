library ieee;
use ieee.std_logic_1164.all;

library pacotes;
use pacotes.pack.all;

-- utiliza a entrada da placa para decidir o modo de jogo
entity mode_decoder is
	port (
		--test
		--clk27M: in std_logic;
		clk_game: in std_logic; -- clock do game
		game_mode: out std_logic; -- modo de jogo? 0: player vs player, 1: player vs AI
		first_player: out std_logic; -- primeiro jogador? 0: esquerda, 1: direita
		
		en_decoder: in std_logic; -- enable para o decoder
		sw_mode: in std_logic; -- 0 player vs player, 1: player vs AI
		sw_player:in std_logic -- primeiro jogador? 0: esquerda, 1: direita
	);
end mode_decoder;

architecture behaviour of mode_decoder is
	--signal clk_game: std_logic;
begin
	--clk_game <= clk27M;
	process(clk_game)
	begin
		if(rising_edge(clk_game)) then
			if(en_decoder = '1') then
				game_mode <= sw_mode;
				first_player <= sw_player;
			end if;
		end if;
	end process;
end behaviour;