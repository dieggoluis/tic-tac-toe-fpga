library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library pacotes;
use pacotes.pack.all;

-- solicita uma jogada e retorna a validade, vez e as pontuacoes de cada jogador
entity game_engine is
	port (
		clear: in std_logic;
		
		clk_game: in std_logic; -- clock do game
		mode: in std_logic;
		game_board: in array_2d;
		AI_r, AI_c, player_r, player_c: in integer range 0 to 3; -- linha/coluna do tabuleiro
		first_player: in std_logic; -- primeiro jogador: 0
									-- segundo jogador: 1
		valid: out std_logic; -- jogada valida: 1
							   -- jogada invalida: 0
							   
		r,c: out integer range 0 to 3; -- linha/coluna que ser�o inseridas
		player_move: out std_logic;
		
		turn: out std_logic; -- de qual jogador � a vez
		
		--debug
		game_result: std_logic_vector(1 downto 0);
		turn_cnt: out std_logic_vector(3 downto 0)
	);
end game_engine;

architecture behaviour of game_engine is
begin
	process(clk_game)
	variable v_turn: std_logic;
	variable v_turn_cnt: integer range 0 to 15;
	variable v_r,v_c: integer range 0 to 3;
	variable v_score_p1,v_score_p2: integer range 0 to 9;
	begin
		if clk_game = '1' and clk_game'EVENT then
			if clear = '1' then
				v_turn := first_player;
				if(first_player = '0') then
					v_turn_cnt := 0;
				else
					v_turn_cnt := 1;
				end if;
			else
				valid <= '0'; -- sup�e que a jogada � inv�lida
				
				if v_turn = '0' or mode = '0'	then -- copia a jogada do jogador humano
					v_r := player_r;
					v_c := player_c;
				else -- copia a jogada do AI
					v_r := AI_r;
					v_c := AI_c;
				end if;
				
				if v_r /= 3 and v_c /= 3 and -- verifica se a jogava � v�lida
						game_board(v_r,v_c) = "00" then -- e se a posi��o est� livre
					valid <= '1'; --a jogada se torna v�lida
					
					r <= v_r;
					c <= v_c;
					player_move <= v_turn;
					v_turn := not v_turn;
					v_turn_cnt := v_turn_cnt + 1;
				end if;							
			end if;
			turn <= v_turn;
			turn_cnt <= std_logic_vector(to_unsigned(v_turn_cnt,4));
		end if;
	end process;
end behaviour;	
