LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY pacotes;
use pacotes.pack.all;
use pacotes.custom_types.all;

PACKAGE pack IS
	type array_2d is array(2 downto 0, 2 downto 0) of std_logic_vector(1 downto 0);
	type pair_ii is array(1 downto 0) of integer range 0 to 3;
	type array_pair_ii is array(2 downto 0) of pair_ii;
	
	component board_decoder is
	port (
		clk_game: in std_logic; -- clock do game
		game_mode: in std_logic; -- Modo de jogo: 0-Jogador vs Jogador
								 --				  1-Jogador vs AI
		en_result: in std_logic; 
		en_mode: in std_logic;    -- Habilita/desabilita chave que indica modo do jogo
		turn: in std_logic; -- Vez: 0- Primeiro Jogador, 1- Segundo jogador
		winner: in std_logic_vector(1 downto 0); -- Vencedor: 00 - nenhum resultado
												 --		   01 - segundo jogador
											     --		   10 - primeiro jogador
											     --		   11 - empate
		--test
		--clk27M: in std_logic;
		--score_p1,score_p2: in std_logic_vector(3 downto 0); -- pontuacao dos jogadores
		
		-- saidas da placa
		LEDG: out std_logic_vector(7 downto 0); -- leds verdes
		LEDR: out std_logic_vector(7 downto 0); -- leds vermelhos
		HEX0,HEX1,HEX2,HEX3: out std_logic_vector(0 to 6 ) -- displays de 7 segmentos
				
	);
	end component;
	
	component game_engine is
	port (
		clear: in std_logic;
		clk_game: in std_logic; -- clock do game
		mode: in std_logic;
		game_board: in array_2d;
		AI_r, AI_c, player_r, player_c: in integer range 0 to 3; -- linha/coluna do tabuleiro
		first_player: in std_logic; -- primeiro jogador: 0
									-- segundo jogador: 1
		valid: out std_logic; -- jogada valida: 1
							   -- jogada invalida: 0
							   
		r,c: out integer range 0 to 3; -- linha/coluna que serao inseridas
		turn: out std_logic; -- de qual jogador � a vez
		score_p1, score_p2: out integer range 0 to 9 -- pontuacao de cada jogador
	);
	end component;
	
	component game_result is
	port (
		clk_game: in std_logic; -- clock do game
		game_board: in array_2d;
		state: out std_logic_vector(1 downto 0)  -- 00 nao houve empate e nem vencedor
												 -- 11 houve empate
												 -- 10 primeiro player venceu
												 -- 01 segundo player venceu
	);
	end component;	
	
	component game_state is
	port (
		clk_game: in std_logic; -- clock do game
		r, c: in integer range 0 to 3; -- linha e coluna 
		turn: in std_logic; -- primeiro jogador (esquerda): 0
							-- segundo jogador (direita): 1
		valid: in std_logic; -- validade da jogada, 0 para inv�lida e 1 para v�lida
		clear: in std_logic; -- limpa o tabuleiro
		
		num_col,num_row: out array_pair_ii;
		num_main_diag, num_sec_diag: out pair_ii;
		
		game_board: out array_2d -- tabuleiro que representa o estado do jogo
		
		
	);
	end component;
	
	component keyboard_decoder is
	port
	(
		------------------------	Clock Input	 	------------------------
		CLOCK_24	: 	in	STD_LOGIC_VECTOR (1 downto 0);	--	24 MHz
									
		------------------------	PS2		--------------------------------
		PS2_DAT 	:		inout	STD_LOGIC;	--	PS2 Data
		PS2_CLK		:		inout	STD_LOGIC;		--	PS2 Clock
		
		event_square: out std_logic;
		event_enter: out std_logic;
		square	: out integer range 0 to 9;
		reset 	: in std_logic;
		pressed : out std_logic;
		en		: in std_logic
		
		-- test
		--clk27M		:   in std_logic;
		--LEDG: out std_logic_vector(7 downto 0);
		--LEDR: out std_logic_vector(7 downto 0)
	);
	end component;
	
	component mode_decoder is
	port (
		--test
		--clk27M: in std_logic;
		clk_game: in std_logic; -- clock do game
		game_mode: out std_logic; -- modo de jogo? 0: player vs player, 1: player vs AI
		resetn: out std_logic; -- botao de reset pressionado: low active
		first_player: out std_logic; -- primeiro jogador? 0: esquerda, 1: direita
		
		en_decoder: in std_logic; -- enable para o decoder
		sw_mode: in std_logic; -- 0 player vs player, 1: player vs AI
		sw_player:in std_logic; -- primeiro jogador? 0: esquerda, 1: direita
		key_resetn: in std_logic -- reset: low active
	);
	end component;
	
	component players is
	port ( 
		 clk_game: in std_logic; -- clock do game
		 square: in integer range 0 to 9; -- quadrado do tabuleiro que foi selecionado
         pressed: in std_logic;               -- pressed: 1
                                              -- not pressed: 0
         r, c: out integer range 0 to 3 -- row and column, respectively
         
         --test 
         --clk27M: in std_logic;
         --LEDG: out std_logic_vector(7 downto 0);
		 --LEDR: out std_logic_vector(7 downto 0);
		 --sq: in std_logic_vector(3 downto 0)
       );
	end component;
	
	component vga_decoder is
	port (
		game_board: in array_2d; -- tabuleiro que representa o estado do jogo
		resetn: in std_logic; -- low active
		en_draw: in std_logic; -- enable draw symbols
		
		-- entradas e saidas da vga
		clk27M: in std_logic;
		VGA_R, VGA_G, VGA_B	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		VGA_HS, VGA_VS		: OUT STD_LOGIC
	);
	end component;
END PACKAGE;