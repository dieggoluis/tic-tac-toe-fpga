library ieee;
use ieee.std_logic_1164.all;

entity clock_divider is
	generic (N: integer := 4);    -- N par -> Clock_out = Clock_in/N
								  -- N impar -> Clock_out = Clock_in/(N-1)
	
	port ( Clock_in: in std_logic;
		   Resetn: in std_logic;
		   Clock_out: out std_logic );
end clock_divider;

architecture Behavior of clock_divider is
	signal Clock_read: std_logic;
begin
	process(Clock_in, Resetn)
		variable count: integer range 0 to N/2;  
	begin
		if Resetn = '0' then
			count := 0;
			Clock_read <= '0';
		elsif Clock_in'Event and Clock_in = '1' then
			if count = N/2 then
				count := 0;
				Clock_read <= not Clock_read;
			end if;
			count := count + 1;
		end if;
	end process;	
	
	Clock_out <= Clock_read;
	
end Behavior;