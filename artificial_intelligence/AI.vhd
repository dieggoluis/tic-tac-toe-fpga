library ieee;
use ieee.std_logic_1164.all;

library pacotes;
use pacotes.pack.all;

-- analisa o tabuleiro e define a proxima jogada do computador
entity AI is
	port (
		clk_game: in std_logic; -- clock do game
		game_board: in array_2d; -- representa o tabuleiro 3x3
		
		-- contagem de pe�as
		num_col,num_row: in array_pair_ii; 
		num_main_diag, num_sec_diag: in pair_ii;
		
		r, c: out integer range 0 to 3 -- linha e coluna, respectivamente
	);
end AI;
	
architecture behaviour of AI is
begin
	-- Conven��es: Em geral, linha pode ser substituido por coluna ou diagonal.
	-- Uma linha � dominada por um jogador se possui apenas duas pe�as do jogador e nenhuma do oponente
	-- Uma linha est� parcialmente dominada se possui apenas uma pe�a de um jogador e nenhuma do oponente
	-- As linhas s�o varridas na seguinte ordem: horizontal, vertical, diagonal
	
	
	-- BUG: dar prioridade para vencer ( dividir a heuristica em duas )
	process(clk_game)
	variable done: std_logic;
	variable tmp : std_logic;
	begin
		if clk_game'EVENT and clk_game = '1' then
	
			for i in 0 to 1000000 loop
				tmp :=  '1';
			end loop;
	
	
			done := '0';
			r <= 3;
			c <= 3;
			-- Heuristica 1: Vencer!
			-- Verifica se alguma linha est� dominada pelo pela AI e garante a vit�ria
			
			-- -- loop por linha ( horizontal)
			 for i in 0 to 2 loop
				 if    (num_row(i)(1) = 2 and num_row(i)(0) = 0) then -- dominada pela AI
					 for j in 0 to 2 loop -- encontra a posi��o vazia ( �nica)
						 if game_board(i,j) = "00" then
							 r <= i;
							 c <= j;
							 done := '1';
						 end if;
					 end loop;
				 end if;
			 end loop;
			
			 --loop por coluna ( vertical)
			 if done = '0' then
				 for i in 0 to 2 loop
					 if  (num_col(i)(1) = 2 and num_col(i)(0) = 0) then -- dominada pelo AI
						 for j in 0 to 2 loop -- encontra a posi��o vazia ( �nica)
							 if game_board(j,i) = "00" then
								 r <= j;
								 c <= i;
								 done := '1';
							 end if;
						 end loop;
					 end if;
				 end loop;
			 end if;
			
			 -- diagonal principal
			 if done = '0' and (num_main_diag(1) = 2 and num_main_diag(0) = 0) then
				 for i in 0 to 2 loop
					 if game_board(i,i) = "00" then
								 r <= i;
								 c <= i;
								 done := '1';
							 end if;
				 end loop;
			 end if;
			
			 -- diagonal secund�ria
			 if done = '0' and  (num_sec_diag(1) = 2 and num_sec_diag(0) = 0) then
				 for i in 0 to 2 loop
					 if game_board(i,2-i) = "00" then
								 r <= i;
								 c <= 2-i;
								 done := '1';
							 end if;
				 end loop;
			 end if;
			
			 -- Heuristica 2: n�o perder!
			 -- Verifica se alguma linha est� dominada pelo oponente
			
			 -- loop por linha ( horizontal)
			 if done = '0' then
				 for i in 0 to 2 loop
					 if    (num_row(i)(1) = 0 and num_row(i)(0) = 2) then -- dominada pelo jogador
						 for j in 0 to 2 loop -- encontra a posi��o vazia ( �nica)
							 if game_board(i,j) = "00" then
								 r <= i;
								 c <= j;
								 done := '1';
							 end if;
						 end loop;
					 end if;
				 end loop;
			 end if;
			
 --			 loop por coluna ( vertical)
			 if done = '0' then
				 for i in 0 to 2 loop
					 if    (num_col(i)(1) = 0 and num_col(i)(0) = 2) then -- dominada pelo jogador
						 for j in 0 to 2 loop -- encontra a posi��o vazia ( �nica)
							 if game_board(j,i) = "00" then
								 r <= j;
								 c <= i;
								 done := '1';
							 end if;
						 end loop;
					 end if;
				 end loop;
			 end if;
			
			 -- diagonal principal
			 if done = '0' and (num_main_diag(1) = 0 and num_main_diag(0) = 2) then
				 for i in 0 to 2 loop
					 if game_board(i,i) = "00" then
								 r <= i;
								 c <= i;
								 done := '1';
							 end if;
				 end loop;
			 end if;
			
			 -- diagonal secund�ria
			 if done = '0' and (num_sec_diag(1) = 0 and num_sec_diag(0) = 2) then
				 for i in 0 to 2 loop
					 if game_board(i,2-i) = "00" then
								 r <= i;
								 c <= 2-i;
								 done := '1';
							 end if;
				 end loop;
			 end if;
			
			 -- Heuristica 3: Atacar
			 -- Verifica se existe uma linha parcialmente dominada e a torna dominada
			
			 -- loop por linha (horizontal)
			 if done = '0' then
				 for i in  0 to 2 loop
					 if num_row(i)(1) = 1 and num_row(i)(0) = 0 then
						 for j in 0 to 2 loop -- encontra a primeira posi��o vazia
							 if game_board(i,j) = "00" then
								 r <= i;
								 c <= j;
								 done := '1';
							 end if;
						 end loop;
					 end if;
				 end loop;
			 end if;
			
			 --loop por coluna ( vertical)
			 if done = '0' then
				 for i in 0 to 2 loop
					 if  (num_col(i)(1) = 1 and num_col(i)(0) = 0) then -- dominada pelo jogador
						 for j in 0 to 2 loop -- encontra a posi��o vazia ( �nica)
							 if game_board(j,i) = "00" then
								 r <= j;
								 c <= i;
								 done := '1';
							 end if;
						 end loop;
					 end if;
				 end loop;
			 end if;
			
			 -- diagonal principal
			 if done = '0' and (num_main_diag(1) = 1 and num_main_diag(0) = 0) then
				 for i in 0 to 2 loop
					 if game_board(i,i) = "00" then
								 r <= i;
								 c <= i;
								 done := '1';
							 end if;
				 end loop;
			 end if;
			
			 -- diagonal secund�ria
			 if done = '0' and  (num_sec_diag(1) = 1 and num_sec_diag(0) = 0) then
				 for i in 0 to 2 loop
					 if game_board(i,2-i) = "00" then
								 r <= i;
								 c <= 2-i;
								 done := '1';
							 end if;
				 end loop;
			 end if;
			-- Heuristica 4: Centro
			-- Verifica se o centro do tabuleiro est� livre e coloca uma pe�a
			if done = '0' and game_board(1,1) = "00" then
				r <= 1;
				c <= 1;
				done := '1';
			end if;
			
			-- Heuristica 5: Cantos
			-- Verifica se um dos cantos do tabuleiro est� livre e coloa uma pe�a
			if done = '0' then
				for i in 0 to 1 loop
					for j in 0 to 1 loop
						if game_board(2*i,2*j) = "00" then
							r <= 2*i;
							c <= 2*j;
							done := '1';
						end if;
					end loop;
				end loop;
			end if;
			-- Movimento aleat�rio
			-- Se nenhuma heur�stica funcionar, coloca uma pe�a na primeira posi��o livre
			if done = '0' then
				for i in 0 to 2 loop
					for j in 0 to 2 loop
						if game_board(i,j) = "00" then
							r <= i;
							c <= j;
						end if;
					end loop;
				end loop;
			end if;
		end if;
	end process;
end behaviour;