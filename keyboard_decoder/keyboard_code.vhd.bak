LIBRARY ieee;
USE ieee.std_logic_1164.all;

library pacotes;
use pacotes.vgaDecoder_package.all;
use pacotes.custom_types.all;

entity keyboard_decoder is
	port
	(
		------------------------	Clock Input	 	------------------------
		CLOCK_24	: 	in	STD_LOGIC_VECTOR (1 downto 0);	--	24 MHz
		CLOCK_27	:		in	STD_LOGIC_VECTOR (1 downto 0);	--	27 MHz
		CLOCK_50	: 	in	STD_LOGIC;											--	50 MHz
		-- CLOCKTAP	: 	out	STD_LOGIC;											
		
		------------------------	7-SEG Dispaly	------------------------
		HEX0 	:		out	STD_LOGIC_VECTOR (0 to 6);		--	Seven Segment Digit 0
		HEX1 	:		out	STD_LOGIC_VECTOR (0 to 6);		--	Seven Segment Digit 1
		HEX2 	:		out	STD_LOGIC_VECTOR (0 to 6);		--	Seven Segment Digit 2
		HEX3 	:		out	STD_LOGIC_VECTOR (0 to 6);		--	Seven Segment Digit 3
		
		----------------------------	LED		----------------------------
		LEDG 	:		out	STD_LOGIC_VECTOR (7 downto 0);		--	LED Green[7:0]
		LEDR 	:		out	STD_LOGIC_VECTOR (9 downto 0);		--	LED Red[9:0]
					
		------------------------	PS2		--------------------------------
		PS2_DAT 	:		inout	STD_LOGIC;	--	PS2 Data
		PS2_CLK		:		inout	STD_LOGIC;		--	PS2 Clock
		
		-- entradas e saidas da vga
		clk27M: in std_logic;
		VGA_R, VGA_G, VGA_B	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		VGA_HS, VGA_VS		: OUT STD_LOGIC;
		
		signal reset 	: in std_logic
	);
end keyboard_decoder;

architecture struct of keyboard_decoder is
	component conv_7seg
		port(
			digit		:		in STD_LOGIC_VECTOR (3 downto 0);
			seg			:		out STD_LOGIC_VECTOR (6 downto 0)
		);
	end component;

	component kbdex_ctrl
		generic(
			clkfreq : integer
		);
		port(
			ps2_data	:	inout	std_logic;
			ps2_clk		:	inout	std_logic;
			clk				:	in 	std_logic;
			en				:	in 	std_logic;
			resetn		:	in 	std_logic;
			lights		: in	std_logic_vector(2 downto 0); -- lights(Caps, Nun, Scroll)		
			key_on		:	out	std_logic_vector(2 downto 0);
			key_code	:	out	std_logic_vector(47 downto 0)
		);
	end component;
		
	signal key0 				: std_logic_vector(15 downto 0);
	signal lights, key_on		: std_logic_vector( 2 downto 0);
	
	-- test 
	signal game_board: array_2d;
begin 

	hexseg0: conv_7seg port map(
		key0(3 downto 0), HEX0
	);
	hexseg1: conv_7seg port map(
		key0(7 downto 4), HEX1
	);
	hexseg2: conv_7seg port map(
		key0(11 downto 8), HEX2
	);
	hexseg3: conv_7seg port map(
		key0(15 downto 12), HEX3
	);

	kbd_ctrl : kbdex_ctrl generic map(24000) port map(
		PS2_DAT, PS2_CLK, CLOCK_24(0), '1', reset, lights(1) & lights(2) & lights(0), -- 5 parametro eh o enable
		key_on, key_code(15 downto 0) => key0
	);
	
	LEDG(7 downto 5) <= key_on;
	LEDR(7 downto 5) <= lights;
	
	vga: vga_decoder port map (
		game_board => game_board,
		resetn => reset,
		clk27M => clk27M,
		VGA_R => VGA_R,
		VGA_G => VGA_G,
		VGA_B => VGA_B, 
		VGA_HS => VGA_HS,
		VGA_VS => VGA_VS
	);
	
	with key0(7 downto 0) select
		game_board(0, 0) <= "10" when "01101010",
							"00" when others;
	--	test vga_decoder
	--game_board(0, 0) <= "10";
	game_board(0, 1) <= "10";
	game_board(0, 2) <= "01";
	game_board(1, 0) <= "01";
	game_board(1, 1) <= "01";
	game_board(1, 2) <= "10";
	game_board(2, 0) <= "01";
	game_board(2, 1) <= "10";
	game_board(2, 2) <= "10";
	
end struct;
