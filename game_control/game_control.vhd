------------------------------------------------------------------------------------
------- 							TOP - LEVEL								--------
------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all ;

library pacotes;
use pacotes.pack.all;

-- controla a sequencia de estados do jogo
entity game_control is
	port (
				------------------------	Clock Input	 	------------------------
		
		clk27M		: 	in std_logic;
		CLOCK_24	:  in std_logic_vector(1 downto 0);	
		
		------------------------	Push Button		------------------------
		KEY 	:		in	STD_LOGIC_VECTOR (3 downto 0);		--	Pushbutton[3:0]

		------------------------	DPDT Switch		------------------------
		SW 	:		in	STD_LOGIC_VECTOR (9 downto 0);			--	Toggle Switch[9:0]
		
		------------------------	7-SEG Dispaly	------------------------
		HEX0 	:		out	STD_LOGIC_VECTOR (0 to 6);		--	Seven Segment Digit 0
		HEX1 	:		out	STD_LOGIC_VECTOR (0 to 6);		--	Seven Segment Digit 1
		HEX2 	:		out	STD_LOGIC_VECTOR (0 to 6);		--	Seven Segment Digit 2
		HEX3 	:		out	STD_LOGIC_VECTOR (0 to 6);		--	Seven Segment Digit 3
		
		----------------------------	LED		----------------------------
		LEDG 	:		out	STD_LOGIC_VECTOR (7 downto 0);		--	LED Green[7:0]
		LEDR 	:		out	STD_LOGIC_VECTOR (9 downto 0);		--	LED Red[9:0]
					
		------------------------	PS2		--------------------------------
		PS2_DAT 	:		inout	STD_LOGIC;	--	PS2 Data
		PS2_CLK		:		inout	STD_LOGIC;		--	PS2 Clock
		
		------------------------	VGA		--------------------------------
		VGA_R, VGA_G, VGA_B	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		VGA_HS, VGA_VS		: OUT STD_LOGIC;
		
		signal soft_resetn: in std_logic; 
		signal resetn: in std_logic
	);
end game_control;

architecture behaviour of game_control is

	signal game_board: array_2d;    -- tabuleiro
	signal en_draw: std_logic;		-- enable para os processos que desenham os circulos
	signal is_drawing: std_logic;	-- esta desenhando ? sim 1, nao 0

	signal en_result: std_logic;    -- enable para para mostrar WINS ou TIE na placa
	signal game_mode: std_logic;	-- modo do game, 0: player vs player, 1: AI vs player
	signal en_mode: std_logic;    	-- enable para permitir a mudança de modo de jogo
	signal turn: std_logic;			-- jogador da vez
	
	signal clear_game: std_logic;         			 -- zera o tabuleiro
	signal AI_r, AI_c: integer range 0 to 3;		 -- jogada da AI
	signal player_r, player_c: integer range 0 to 3; -- jogada do player
	signal first_player: std_logic; 				 -- primeiro jogador da partida
	signal valid: std_logic; 	   					 -- jogada valida ?	
	signal r, c: integer range 0 to 3; 				 -- linha e coluna de uma jogada valida
	signal score_p1, score_p2: std_logic_vector(3 downto 0); -- pontuacao de cada jogador
	
	signal result: std_logic_vector(1 downto 0);     -- 00 nao houve empate e nem vencedor
													 -- 11 houve empate
													 -- 10 player 0 venceu
												     -- 01 player 1 venceu


	signal num_col, num_row: array_pair_ii;			 -- numero de peças (simbolos) em cada linha  de cada player
	signal num_main_diag, num_sec_diag: pair_ii;	 -- numero de peças (simbolos) nas diagonais de cada player

	signal event_square : std_logic;			     -- Algum quadrado/tecla do teclado numerico foi pressionado ?
	signal event_enter  : std_logic;				 -- Tecla Enter pressionada ?
	signal square		: integer range 0 to 9;		 -- Quadrado correspondente a jogada ( 0 a 8 )
	signal pressed		: std_logic;				 -- alguma tecla pressionada ?	

	signal en_decoder: std_logic; 	-- enable para o decoder
	signal sw_mode: std_logic; 		-- 0 player vs player, 1: player vs AI
	signal sw_player: std_logic; 	-- switch para escolha do primeiro jogador? 0: esquerda, 1: direita

	signal clock_game: std_logic;		-- clock do jogo
	signal player_move: std_logic;		-- vez de outro player ?
	signal event_result: std_logic;		-- houve alguma vitoria/empate ?
	signal turn_cnt: std_logic_vector(3 downto 0);  -- turn_cnt(0) eh o jogador que tem a vez
	signal global_resetn: std_logic;    -- resetn global da maoria dos modulos
	
		-- Fsm
	type state_type is (CHOOSE_MODE,
						DISPLAY_BOARD,
						WAIT_ENTER,
						INIT_DRAW,
						SHOW_SCREEN,
						UPDATE,
						DECIDE,
						F_PLAYER,
						S_PLAYER,
						WAIT_PLAYER,
						FINISH
	);

	-- Register to hold the current state
	signal state   : state_type;
	
begin
	-- instancia components
	vga_component: vga_decoder port map (
		game_board => game_board,
		resetn     => global_resetn,
		en_draw	   => en_draw,
		is_drawing => is_drawing,
		CLOCK_24   => CLOCK_24,
		clk27M     => clk27M,
		VGA_R      => VGA_R,
		VGA_G      => VGA_G,
		VGA_B      => VGA_B,
		VGA_HS     => VGA_HS,
		VGA_VS     => VGA_VS
	);

	board_component: board_decoder port map (
		clk_game  => clock_game,
		game_mode => game_mode,
		en_result => en_result,
		en_mode   => en_mode,  
		turn	  => turn_cnt(0),
		winner    => result,
		LEDG      => LEDG,
		LEDR      => LEDR(7 downto 0),
		HEX0	  => HEX0,
		HEX1	  => HEX1,
		HEX2	  => HEX2,
		HEX3	  => HEX3,
		score_p1  => score_p1,
		score_p2  => score_p2
	);
	
	engine_component: game_engine port map (
		clear		 => clear_game,
		clk_game	 => clock_game,
		mode 		 => game_mode,
		game_board 	 => game_board,
		AI_r 		 => AI_r,
		AI_c 		 => AI_c,
		player_r 	 => player_r,
		player_c 	 => player_c,
		first_player => first_player,
		player_move  => player_move,
		valid 		 => valid,	   
		r 			 => r,
		c 			 => c,
		turn 		 => turn,
		turn_cnt     => turn_cnt
	);
	
	result_component: game_result port map(
		clk_game   =>  clock_game,
		game_board => game_board,
		state      => result
	);
	
	state_component: game_state port map(
		clk_game      => CLOCK_24(0),
		r             => r,
		c             => c,
		turn          => player_move,
		valid         => valid,
		clear         => clear_game,
		num_row       => num_row,	
		num_col       => num_col,
		num_main_diag => num_main_diag,
		num_sec_diag  => num_sec_diag,
		game_board    => game_board
	);	
	
	keyboard_component: keyboard_decoder port map (			
		CLOCK_24     => CLOCK_24,								
		PS2_DAT      => PS2_DAT,
		PS2_CLK	     => PS2_CLK,
		event_square => event_square,
		event_enter  => event_enter,
		square       => square,
		reset        => global_resetn,
		pressed      => pressed,
		en           => '1'
	);
	
	mode_component: mode_decoder port map (
		clk_game     => clock_game, 
		game_mode    => game_mode,
		first_player => first_player,
		en_decoder   => en_decoder,
		sw_mode      => sw_mode,
		sw_player    => sw_player
	);
	
	player_component: players port map ( 
		clk_game => clock_game,
		square  => square,
		pressed => pressed,
		r       => player_r,
		c       => player_c
	);
	
	AI_component: AI port map (
		clk_game      => clock_game,
		game_board    => game_board,
		num_col       => num_col,
		num_row       => num_row, 
		num_main_diag => num_main_diag,
		num_sec_diag  => num_sec_diag,
		r             => AI_r,
		c             => AI_c
	);
	
	clockGame: clock_divider
		generic map (N => 10)
		port map ( 
		   Clock_in  => CLOCK_24(0),
		   Resetn    => global_resetn,
		   Clock_out => clock_game
		);
		
	-- Logic to advance to the next state
	process (clock_game, resetn, soft_resetn)
	begin
		if resetn = '0' or soft_resetn = '0' then 
			en_decoder <= '0';
			en_mode <= '0';
			clear_game <= '1';
			state <= CHOOSE_MODE;			
		elsif (rising_edge(clock_game)) then
			case state is
				-- escolhe modo
				when CHOOSE_MODE =>
					en_decoder <= '1';
					en_mode <= '1';
					state <= WAIT_ENTER;
				
				-- espera player pressionar Enter para confirmar as opcoes escolhidas	
				when WAIT_ENTER =>
					if(event_enter = '1') then		
						state <= DISPLAY_BOARD;
					else
						state <= WAIT_ENTER;
					end if;			
				
				-- Mostra opcoes escolhidas na placa
				when DISPLAY_BOARD =>
					clear_game <= '0';
					en_decoder <= '0';
					en_mode <= '0';
					state <= INIT_DRAW;
						
				-- Comeca desenhar simbolos
				when INIT_DRAW =>
					state <= SHOW_SCREEN;
				
				-- Enquanto estiver desenhando nao sai do estado
				when SHOW_SCREEN =>
					if(is_drawing = '1') then
						state <= SHOW_SCREEN;
					else
						state <= UPDATE;
					end if;

				-- Atualiza dados	
				when UPDATE =>
					state <= DECIDE;
				
				-- Verifica se houve vitoria/empate ou nada e decide qual proximo estado
				when DECIDE =>				
					if(result = "00") then
						if(turn_cnt(0) = '0') then 
							state <= F_PLAYER;
						else 
							state <= S_PLAYER;
						end if;
					else 
						state <= FINISH; -- partida terminou
					end if;
					
				-- primeiro player - humano
				when F_PLAYER =>
					state <= WAIT_PLAYER;
					
				when WAIT_PLAYER =>
					if(event_square = '1') then
						state <= INIT_DRAW;
					else
						state <= WAIT_PLAYER;
					end if;
					
				when S_PLAYER =>
					if(game_mode = '0') then -- segunda player humano
						state <= F_PLAYER;
					else -- cpu
						state <= INIT_DRAW; -- segunda player maquina
					end if;

				-- partida terminou, mostra resultados
				when FINISH =>
					en_result <= '1';
			end case;
		end if;
	end process;

	-- atualiza pontuacao de cada player
	event_result <= '0' when result = "00" else '1';
	process(event_result, resetn)
	begin
		if(resetn = '0') then
			score_p1 <= "0000";
			score_p2 <= "0000";
		elsif(rising_edge(event_result)) then
			if result = "01" then
				score_p1 <= score_p1 + 1;
			elsif result = "10" then
				score_p2 <= score_p2 + 1;
			end if;
		end if;
	end process;

	-- espera desenhar ultimo simbolo
	process(clock_game, resetn, soft_resetn)
		variable count: integer range 0 to 100000;
	begin
		if(global_resetn = '0') then
			count := 0;
			en_draw <= '1';
		elsif(rising_edge(clock_game)) then
			if(result/="00") then
				if(count < 100000) then
					count := count + 1;
				else
					en_draw <= '0';
				end if;
			end if;
		end if;
	end process;
	
	global_resetn <= resetn and soft_resetn;
	sw_mode <= SW(0);
	sw_player <= SW(1);
	LEDR(9) <= event_enter;

	--test
--    LEDR(6) <= en_draw;
	--LEDG(7) <= event_square;
   	-- LEDG(6) <= score_p1(0);
    --LEDG(6) <= '1' when num_sec_diag(1)=1 else '0';
    --LEDG(5) <= '1' when num_sec_diag(1)=2 else '0';
    --LEDR(6) <= '1' when num_sec_diag(0)=0 else '0';
    --LEDR(5) <= '1' when num_sec_diag(0)=1 else '0';
    --LEDR(4) <= '1' when num_sec_diag(0)=2 else '0';
end behaviour;