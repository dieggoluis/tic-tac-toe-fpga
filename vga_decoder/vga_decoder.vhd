library ieee;
use ieee.std_logic_1164.all;

library pacotes;
use pacotes.pack.all;

-- Imprime o game no monitor
entity vga_decoder is
	port (
		game_board: in array_2d; -- tabuleiro que representa o estado do jogo
		resetn: in std_logic; -- low active
		en_draw: in std_logic; -- enable draw symbols
		is_drawing: out std_logic;
		CLOCK_24: in std_logic_vector(1 downto 0);
		
		-- entradas e saidas da vga
		clk27M: in std_logic;
		VGA_R, VGA_G, VGA_B	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		VGA_HS, VGA_VS		: OUT STD_LOGIC
	);
end vga_decoder;

architecture behaviour of vga_decoder is
	-- test 
	-- signal game_board: array_2d;
	
	-- vgacon component
	COMPONENT vgacon IS
    GENERIC (
      NUM_HORZ_PIXELS : NATURAL := 128;	-- Number of horizontal pixels
      NUM_VERT_PIXELS : NATURAL := 96	-- Number of vertical pixels
      );
    PORT (
      clk27M, rstn              : IN STD_LOGIC;
      write_clk, write_enable   : IN STD_LOGIC;
      write_addr                : IN INTEGER RANGE 0 TO NUM_HORZ_PIXELS * NUM_VERT_PIXELS - 1;
      data_in                   : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
      red, green, blue          : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
      hsync, vsync              : OUT STD_LOGIC
      );
	END COMPONENT;

	CONSTANT HORZ_SIZE : INTEGER := 160;
	CONSTANT VERT_SIZE : INTEGER := 120;
	CONSTANT SYMBOLS_SIZE: INTEGER := 20;
	CONSTANT MEMSIZE : INTEGER := HORZ_SIZE * VERT_SIZE;
	CONSTANT GREEN: STD_LOGIC_VECTOR := "010";
	CONSTANT RED: STD_LOGIC_VECTOR := "100";
	CONSTANT BLACK: STD_LOGIC_VECTOR := "000";

	signal en_symbols: std_logic_vector(1 downto 0) := "11"; -- 00 black square, 01 red X, 10 green O
	signal video_word, color_o, color_x: std_logic_vector(2 downto 0);
	
	SIGNAL address_x,
		   address_o,
		   address_square,
		   addr_init,
		   video_address	: INTEGER := MEMSIZE - 1;
		   
	signal is_draw_x, is_draw_o, is_draw_sq: std_logic; -- Algum simbolo esta sendo desenhado ?
														-- 1 sim, 0 nao

	-- Build an enumerated type for the state machine
	type state_type is (FETCH, DRAW, FINISH, ENABLE);

	-- Register to hold the current state
	signal state   : state_type;

	
begin
	vga_component: vgacon
    GENERIC MAP (
      NUM_HORZ_PIXELS => HORZ_SIZE,
      NUM_VERT_PIXELS => VERT_SIZE
      ) PORT MAP (
        clk27M			=> clk27M		,
        rstn			=> resetn		,
        write_clk		=> clk27M		,
        write_enable	=> '1'			,
        write_addr      => video_address,
        data_in         => video_word	,
        red				=> VGA_R		,
        green			=> VGA_G		,
        blue			=> VGA_B		,
        hsync			=> VGA_HS		,
        vsync			=> VGA_VS		
	);

	with en_symbols select 
		video_word <= color_o when "01",
					  color_x when "10",
					  BLACK when others;
					  
	with en_symbols select
		video_address <= address_o when "01",
						 address_x when "10",
						 address_square when others;
--	test vga_decoder
--	game_board(0, 0) <= "10";
--	game_board(0, 1) <= "10";
--	game_board(0, 2) <= "01";
--	game_board(1, 0) <= "01";
--	game_board(1, 1) <= "01";
--	game_board(1, 2) <= "10";
--	game_board(2, 0) <= "01";
--	game_board(2, 1) <= "10";
--	game_board(2, 2) <= "10";

	-- Logic to advance to the next state
	process (CLOCK_24(0), resetn)
		variable sq: integer range 1 to 9;
	begin
		if resetn = '0' then
			en_symbols <= "11";
			state <= FETCH;
			sq := 1;
		elsif (rising_edge(clk27M)) then
			case state is
				when FETCH=> -- atualiza endereco inicial conforme a posicao corrente
					case sq is
						when 1 => addr_init <= 1617;
						when 2 => addr_init <= 1671;
						when 3 => addr_init <= 1725; 
						when 4 => addr_init <= 8017;
						when 5 => addr_init <= 8071;
						when 6 => addr_init <= 8125;
						when 7 => addr_init <= 14417;
						when 8 => addr_init <= 14471;
						when 9 => addr_init <= 14525;
					end case;
					state <= ENABLE;
				when ENABLE=> -- en_symbols correspondente ao codigo do simbolo
					case sq is
						when 1 => en_symbols <= game_board(0,0);
						when 2 => en_symbols <= game_board(0,1);
						when 3 => en_symbols <= game_board(0,2);
						when 4 => en_symbols <= game_board(1,0);
						when 5 => en_symbols <= game_board(1,1);
						when 6 => en_symbols <= game_board(1,2);
						when 7 => en_symbols <= game_board(2,0);
						when 8 => en_symbols <= game_board(2,1);
						when 9 => en_symbols <= game_board(2,2);
					end case;
					if sq = 9 then sq := 1; else sq := sq + 1; 
					end if;
					state <= DRAW;
				when DRAW=> -- desenha simbolo
					state <= FINISH;
				when FINISH=> -- simbolo foi desenhado
					if is_draw_x = '1' or is_draw_o='1' or is_draw_sq='1' then
						state <= FINISH;
					else 
						en_symbols <= "11";
						state <= FETCH;
					end if;
			end case;
		end if;
	end process;
	
	-- Desenha simbolo X
	draw_x:
	process(CLOCK_24(0))
		variable x, y: integer range 0 to 30;
	begin
		if(rising_edge(clk27M)) then
			if(en_draw='1' and en_symbols = "10") then
				if(address_x < SYMBOLS_SIZE*HORZ_SIZE + SYMBOLS_SIZE + addr_init) then
					is_draw_x <= '1';
					if x = y or x + y = (SYMBOLS_SIZE-1) then
						color_x <= RED;
					else 
						color_x <= BLACK;
					end if;					
					if x = SYMBOLS_SIZE then
						address_x <= address_x + HORZ_SIZE - SYMBOLS_SIZE;
						y := y + 1;
						x := 0;
					else
						address_x <= address_x + 1;
						x := x + 1;
					end if;
				else
					is_draw_x <= '0';
				end if;
			else
				x := 0;
				y := 0;
				is_draw_x <= '0';
				address_x <= addr_init;
			end if;
		end if;
	end process draw_x;
	
	-- Desenha simbolo O
	draw_O:
	process(CLOCK_24(0))
		variable x, y: integer range 0 to 30;
		variable radius: integer range 0 to 160;
	begin
		if(rising_edge(clk27M)) then
			if(en_draw='1' and en_symbols = "01") then
				if(address_o < SYMBOLS_SIZE*HORZ_SIZE + SYMBOLS_SIZE + addr_init) then
					is_draw_o <= '1';
					if x = SYMBOLS_SIZE then
						address_o <= address_o + HORZ_SIZE - SYMBOLS_SIZE;
						y := y + 1;
						x := 0;
					else
						address_o <= address_o + 1;
						x := x + 1;
					end if;
					radius := (x-10)*(x-10) + (y-10)*(y-10);
					if radius >= 92 and radius <= 108 then
						color_o <= GREEN;
					else
						color_o <= BLACK;
					end if;
				else
					is_draw_o <= '0';
				end if;
			else
				x := 0;
				y := 0;
				address_o <= addr_init;
				is_draw_o <= '0';
			end if;
		end if;
	end process draw_O;

	-- Desenha um black square :D
	draw_square:
	process(CLOCK_24(0))
		variable x: integer range 0 to 30;
	begin
		if(rising_edge(clk27M)) then
			if(en_draw='1' and en_symbols = "00") then
				if(address_square < SYMBOLS_SIZE*HORZ_SIZE + SYMBOLS_SIZE + addr_init) then
					is_draw_sq <= '1';
					if x = SYMBOLS_SIZE then
						address_square <= address_square + HORZ_SIZE - SYMBOLS_SIZE;
						x := 0;
					else
						address_square <= address_square + 1;
						x := x + 1;
					end if;
				else
					is_draw_sq <= '0';
				end if;
			else
				x := 0;
				address_square <= addr_init;
				is_draw_sq <= '0';
			end if;
		end if;
	end process draw_square;	
	
	is_drawing <= is_draw_x or is_draw_o or is_draw_sq;
	
end behaviour;